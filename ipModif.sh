#!/usr/bin/env bash

verificacion(){
	echo "[*] Verificando conexión" && sleep 0.5
	CONNECT=`ping -c 1 -w2 $1 | grep "from"`	
}

conexionSSH(){
	if [ "$(command -v tmux)" ]; then
		echo " [*] TMUX está instalado "
		tiempo=1
		tmux new-session -d -t "GNS3VM" && sleep $tiempo
		tmux rename-window "SSH" && tmux select-pane -t 1
		echo " [*] Se está cargando el archivo script.sh"
		tmux send-keys "scp -o StrictHostKeyChecking=no script.sh gns3@$1:/tmp/" C-m && sleep $tiempo
		tmux send-keys "gns3" C-m && sleep $tiempo
		tmux send-keys "ssh -o 'StrictHostKeyChecking no' gns3@$1" C-m && sleep $tiempo
		tmux send-keys "gns3" C-m && sleep $tiempo
		tmux send-keys "^c" C-m && sleep $tiempo
		tmux send-keys "sudo su" C-m && sleep $tiempo
		#Ejecutando el codigo cargado previamente
		echo " [*] Ejecutando el script "
		tmux send-keys "bash /tmp/script.sh $2" C-m && sleep $tiempo
		tmux send-keys "exit" C-m
		tmux send-keys "exit" C-m && sleep $tiempo
		# Cerrando la sesión de TMUX
		tmux send-keys "exit" C-m
		echo "[*] Cambio realizado exitosamente "
	else
		echo " [!] TMUX no está instalado, deberá instalarlo antes... "
	fi

}


#------------------------------------
#Programa principal
#------------------------------------
echo " "
echo "***************************" 
echo "Bienvenido al programa de modificación de IP para GNS3 VM" 
echo "***************************" && sleep 0.5

if [[ $# -eq 2 ]]; then
	#Datos ingresados correctamente

	verificacion $1
	
	# Verificando alcance de la IP
	if [[ -n "$CONNECT" ]]; then
	        echo "[*] Conexión exitosa" && sleep 0.5
	        echo "[*] Se cambiará la IP a $2"  && sleep 0.5
		conexionSSH $1 $2
	else
	       echo "[!] IP sin respuesta"
	fi
else
	echo " [!] Modo de uso: " && sleep 0.5
	echo " ./ipModif.sh <IP_Original> <IP_Nueva>" && sleep 0.5
fi


