#!/usr/bin/env bash
echo "`sudo su`"
echo "`rm /etc/netplan/90_gns3vm_static_netcfg.yaml`"
GATEWAY="`echo "$1" | cut -d"." -f1,2,3 `"

if [[ -n $1 ]]; then
	echo "network:" >> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "  version: 2">> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "  renderer: networkd">> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "  ethernets:">> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "    eth0:">> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "      dhcp4: no">> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "      addresses:">> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "        - $1/24">> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "      gateway4: $GATEWAY.1">> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "      nameservers:">> /etc/netplan/90_gns3vm_static_netcfg.yaml
	echo "        addresses: [8.8.8.8,8.8.4.4]">> /etc/netplan/90_gns3vm_static_netcfg.yaml

	echo "`reboot`"

fi
