# ipModifGNS3

Herramienta ideal para la modificación automatizada de la IP de una máquina virtual GNS3VM a una IP estática definida por el usuario.

Modo de uso
======

Deberá seguirle el siguiente orden
```bash
$ ./ipModif.sh <IP_Actual> <IP_Nueva>
```
Ejemplo:
```bash
$ ./ipModif.sh 192.168.0.105 10.11.4.2
```
¿Cómo funciona?
======
1.- Verifica que se haya ingresado un IP objetivo válida haciendo ping a dicha ip <br>
2.- Se crea una nueva sesión de TMUX (permite iniciar una terminal e ingresar comandos directamente) <br>
3.- Se carga vía SSH el archivo script.sh a la ruta /tmp/ <br>
4.- Conexión vía SSH usando el usuario gns3 <br>
4.- Se ejecuta el script cargado previamente. <br>
5.- Se reinicia la VM para recargar la configuración del nuevo IP <br>

Requisitos
======
Para la conexión a la máquina virtual se usa TMUX, por ello es necesario tenerlo instalado.

